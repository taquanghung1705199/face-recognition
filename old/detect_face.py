# plot photo with detected faces using opencv cascade classifier
import cv2
import time

start_time = time.time()
# load the photograph
str = '/home/hung/git-tutorial/face-recognition/img-origin/biden.jpg'
pixels = cv2.imread(str)
# load the pre-trained model
classifier = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
# perform face detection
bboxes = classifier.detectMultiScale(pixels, 1.1, 3) # default
# bboxes = classifier.detectMultiScale(pixels, 1.05, 8)
# print bounding box for each detected face
for box in bboxes:
	# extract
	x, y, width, height = box
	x2, y2 = x + width, y + height
	# draw a rectangle over the pixels
	cv2.rectangle(pixels, (x, y), (x2, y2), (0,0,255), 1)
# show the image
str2 = '/home/hung/git-tutorial/face-recognition/img-after-detect/'
cv2.imwrite(str2 + 'biden.jpg', pixels)
cv2.imshow('face detection', pixels)
# keep the window open until we press a key
print("--- %s seconds ---" % (time.time() - start_time))
cv2.waitKey(0)
# close the window
cv2.destroyAllWindows()